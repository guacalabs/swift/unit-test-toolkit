import XCTest
import os.log

public func given<T>(_ description: String, _ block: () throws -> T) rethrows -> T {
    os_log("1. Given: %{public}@", description)
    return try XCTContext.runActivity(named: "Given \(description)", block: { _ in
        try block()
    })
}

public func when<T>(_ description: String, _ block: () throws -> T) rethrows -> T {
    os_log("2.  When: %{public}@", description)
    return try XCTContext.runActivity(named: "When \(description)", block: {
        _ in try block()
    })
}

public func then<T>(_ description: String, _ block: () throws -> T) rethrows -> T {
    os_log("3.  Then: %{public}@", description)
    return try XCTContext.runActivity(named: "Then \(description)", block: {
        _ in try block()
    })
}
