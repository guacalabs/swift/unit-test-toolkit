// swift-tools-version:5.1
import PackageDescription

let package = Package(
    name: "UnitTestToolkit",
    platforms: [
        .iOS(.v11),
        .tvOS(.v11),
        .macOS(.v10_14)
    ],
    products: [
        .library(
            name: "UnitTestToolkit",
            targets: ["UnitTestToolkit"]
        )
    ],
    dependencies: [],
    targets: [
        .target(
            name: "UnitTestToolkit",
            dependencies: [],
            path: "./Sources"
        )
    ],
    swiftLanguageVersions: [.v5]
)
