import Foundation
import XCTest

public func XCTAssertExpression<T, E: Error & Equatable>(
    _ expression: @autoclosure () throws -> T,
    throws error: E,
    in file: StaticString = #file,
    line: UInt = #line
) {
    var thrownError: Error?
    XCTAssertThrowsError(try expression(), file: file, line: line) {
        thrownError = $0
    }

    guard let unwrappedError = thrownError else {
        return
    }
    XCTAssertTrue(
        unwrappedError is E,
        "Unexpected error type: \(type(of: unwrappedError)).\(unwrappedError)",
        file: file,
        line: line
    )

    guard let castError = unwrappedError as? E else {
        XCTFail(
            "expected: \(type(of: error)).\(error) - thrown: \(type(of: unwrappedError)).\(unwrappedError)",
            file: file,
            line: line
        )
        return
    }
    XCTAssertEqual(castError, error, file: file, line: line)
}
