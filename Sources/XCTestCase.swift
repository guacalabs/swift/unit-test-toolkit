import XCTest

private var DEFAULT_TIMEOUT = TimeInterval(0.2)

public extension XCTestCase {
    /// The `timeout` set for the expectations.
    /// By default, the value is **0.2**.
    static var timeout: TimeInterval {
        get { DEFAULT_TIMEOUT }
        set { DEFAULT_TIMEOUT = newValue }
    }
}

public extension XCTestCase {
    func waitForExpectations(handler: XCWaitCompletionHandler? = .none) {
        waitForExpectations(timeout: DEFAULT_TIMEOUT, handler: handler)
    }

    func wait(for expectations: [XCTestExpectation], enforceOrder: Bool = false) {
        wait(for: expectations, timeout: DEFAULT_TIMEOUT, enforceOrder: enforceOrder)
    }
}

public extension XCTestCase {
    /// When creating a `spec` we are telling the test that we are expecting this to happen.
    /// - Parameter description: Description of the **expectation**.
    /// - Parameter fulfillmentCount: Number of the expected calls count.
    func spec(_ description: String, fulfillmentCount: Int = 1) -> XCTestExpectation {
        let spec = expectation(description: description)
        spec.expectedFulfillmentCount = 1
        return spec
    }

    /// When creating a `unspec` we are telling the test that we are expecting this to not happen.
    /// - Parameter description: Description of the **not-expectation**.
    func unspec(_ description: String) -> XCTestExpectation {
        let spec = expectation(description: description)
        spec.isInverted = true
        return spec
    }
}
