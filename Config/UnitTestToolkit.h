#import <Foundation/Foundation.h>

//! Project version number for UnitTestToolkit.
FOUNDATION_EXPORT double UnitTestToolkitVersionNumber;

//! Project version string for UnitTestToolkit.
FOUNDATION_EXPORT const unsigned char UnitTestToolkitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <UnitTestToolkit/PublicHeader.h>
